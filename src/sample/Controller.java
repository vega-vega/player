package sample;

import javafx.application.Platform;
import javafx.beans.InvalidationListener;
import javafx.beans.Observable;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.control.Label;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import javafx.scene.media.MediaView;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import javafx.util.Duration;

import java.awt.*;
import java.awt.geom.Arc2D;
import java.io.File;
import java.net.URL;
import java.nio.file.Paths;
import java.util.ResourceBundle;

public class Controller implements Initializable{
    public Label topLabel;
    public ScrollBar volume;
    public Label timeLabel;
    public Slider timeSlider;

    @FXML
    MediaView mediaView;

    private boolean mute = false;
    private boolean repeat = false;
    private MediaPlayer mediaPlayer;
    private FileChooser chooser;
    private static double setVolume = 0.2d;
    private Duration duration;

    static int i = 0;


    @Override
    public void initialize(URL location, ResourceBundle resources) {
        chooser = new FileChooser();
        FileChooser.ExtensionFilter extensionFilter = new FileChooser.ExtensionFilter("audio", "*.mp3", "*.wav", "*.aif" );
        chooser.getExtensionFilters().add(extensionFilter);
    }


    public void replayMusic(ActionEvent actionEvent) {
        if (mediaPlayer != null) {
            mediaPlayer.seek(mediaPlayer.getStartTime());
            mediaPlayer.stop();
        }
    }

    public void backMusic(ActionEvent actionEvent) {
    }


    public void forwardMusic(ActionEvent actionEvent) {
    }

    public void playMusic(ActionEvent actionEvent) {
        if (mediaPlayer != null) {
            mediaPlayer.play();
            String source = mediaPlayer.getMedia().getSource();
            source = source.substring(0, source.length() - 4);
            source = source.substring(source.lastIndexOf("/") + 1).replaceAll("%20", " ");
            topLabel.setText(source);
            duration = mediaPlayer.getMedia().getDuration();

            mediaPlayer.currentTimeProperty().addListener(new InvalidationListener() {
                @Override
                public void invalidated(Observable observable) {
                    Platform.runLater(new Runnable() {
                        @Override
                        public void run() {
                            Duration currentTime = mediaPlayer.getCurrentTime();
                            timeLabel.setText(formatTime(currentTime, duration));
                            timeSlider.setDisable(duration.isUnknown());
                            if (!timeSlider.isDisabled() && duration.greaterThan(Duration.ZERO)
                                    && !timeSlider.isValueChanging()) {
                                timeSlider.setValue(currentTime.divide(duration).toMillis() * 100.0);
                            }
                        }
                    });
                }
            });

            timeSlider.valueProperty().addListener(new InvalidationListener() {
                @Override
                public void invalidated(Observable observable) {
                    if(timeSlider.isValueChanging()) {
                        mediaPlayer.seek(duration.multiply(timeSlider.getValue() / 100.0));
                    }
                }
            });
        }
    }

    public void stopMusic(ActionEvent actionEvent) {
        if (mediaPlayer != null) {
            mediaPlayer.pause();
        }
    }

    public void fileMusic(ActionEvent actionEvent) {
        File file = chooser.showOpenDialog(new Stage());
        if (file != null) {
            Media hit = new Media(file.toURI().toString());
            stopMusic(null);
            mediaPlayer = new MediaPlayer(hit);
            setVolume(setVolume);
            Thread thread = new Thread(new Runnable() {
                @Override
                public void run() {
                    while (mediaPlayer.getStatus() == MediaPlayer.Status.PLAYING) {
                        timeLabel.setText(String.valueOf(mediaPlayer.getCurrentTime()));
                    }
                }
            });
            thread.start();
        }
    }

    public void reloadMusic(ActionEvent actionEvent) {
        if (mediaPlayer != null) {
            mediaPlayer.setOnEndOfMedia(new Runnable() {
                @Override
                public void run() {
                    if (repeat) {
                        mediaPlayer.seek(mediaPlayer.getStartTime());
                    }
                }
            });
        }
        repeat = !repeat;
    }

    public void volumeMusic(ActionEvent actionEvent) {
        if (mute == true) {
            setVolume(setVolume);
        } else {
            setVolume(0.0d);
        }
        mute = !mute;
    }

    public void changeValue(Event event) {
        setVolume = Math.round(volume.getValue())*0.01d;
        setVolume(setVolume);
        mute = false;
    }

    private void setVolume(Double volume){
        if (mediaPlayer != null){
            mediaPlayer.setVolume(volume);
        }
    }

    private static String formatTime(Duration elapsed, Duration duration) {
        int intElapsed = (int) Math.floor(elapsed.toSeconds());
        int elapsedHours = intElapsed / (60 * 60);
        if (elapsedHours > 0) {
            intElapsed -= elapsedHours * 60 * 60;
        }
        int elapsedMinutes = intElapsed / 60;
        int elapsedSeconds = intElapsed - elapsedHours * 60 * 60 - elapsedMinutes
                * 60;

        if (duration.greaterThan(Duration.ZERO)) {
            int intDuration = (int) Math.floor(duration.toSeconds());
            int durationHours = intDuration / (60 * 60);
            if (durationHours > 0) {
                intDuration -= durationHours * 60 * 60;
            }
            int durationMinutes = intDuration / 60;
            int durationSeconds = intDuration - durationHours * 60 * 60
                    - durationMinutes * 60;
            if (durationHours > 0) {
                return String.format("%d:%02d:%02d/%d:%02d:%02d", elapsedHours,
                        elapsedMinutes, elapsedSeconds, durationHours, durationMinutes,
                        durationSeconds);
            } else {
                return String.format("%02d:%02d/%02d:%02d", elapsedMinutes,
                        elapsedSeconds, durationMinutes, durationSeconds);
            }
        } else {
            if (elapsedHours > 0) {
                return String.format("%d:%02d:%02d", elapsedHours, elapsedMinutes,
                        elapsedSeconds);
            } else {
                return String.format("%02d:%02d", elapsedMinutes, elapsedSeconds);
            }
        }
    }
}
